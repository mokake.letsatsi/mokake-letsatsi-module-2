Create a class and a) then use an object to print the name of the app, sector/category, developer, and the year it won MTN Business App of the Year Awards. b) Create a function inside the class, transform the app name to all capital letters and then print the output. 

class App {
   var appName;
   var appCatergory;
   var appDev;
   var appYear;
  
  showAppInfo(){
    print("Application Name Is : ${appName}");
    print("application Catergory Is : ${appCatergory}");
    print("application Developer Is : ${appDev}");
    print("application Won in : ${appYear}");
  }
}
void main(){
  var app = new App();
  app.appName = "Ambani Africa";
  app.appCatergory = "Best Gaming Solution, Best Educational, and Best South African solution";
  app.appDev =  "Mukundi Lamban";
  app.appYear = 2021;
  print("MTN App academy M2-Assessment 1");
  app.showAppInfo();
}
